# oeislib
Simple Python library for OEIS (Online Encyclopedia of Integer Sequences)

Forked from [https://github.com/phantom-5/oeispy](https://github.com/phantom-5/oeispy)

Changes:

* resultEois becomes resultOeis
* start=0 instead of start=1 in resultOeis
* README updates
* add getBfileData method

List of methods with examples (see [https://oeis.org/eishelp2.html](https://oeis.org/eishelp2.html) for more information):

Query OEIS
```sh
import oeislib as op
res=op.resultOeis('1,9,15,19')
```
Best result
```sh
op.topResult(res)
```
Count results
```sh
op.countResult(res)
```
Get number (absolute catalog number is A + number as 6 digits)
```sh
op.getNumber(res[0])
```
Get ID (M- and N- numbers from earlier published listings, if applicable)
```sh
op.getId(res[0])
```
Get data
```sh
op.getData(res[0])
```
Get name
```sh
op.getName(res[0])
```
Get comment
```sh
op.getComment(res[0])
```
Get link
```sh
op.getLink(res[0])
```
Get example
```sh
op.getExample(res[0])
```
Similarly , getAuthor(..), getTime(..), getCreated(..), getFormula(..), getProgram(..)

Get b-file data (generator object yields pairs [i, a(i)])
```sh
op.getBfileData(res[0])
```

Get graph and save image
```sh
op.getGraph('A000001')
```
![](graph.png)

Get random
```sh
res=op.getRandom()
```

