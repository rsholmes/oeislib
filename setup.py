from setuptools import setup,find_packages

classifiers=['Development Status :: Alpha',
             'Intended Auidence :: Education',
             'Operating System :: Microsoft :: Windows :: Windows 10',
             'License :: OSI Approved :: MIT License',
             'Programming Language :: python :: 3']

setup(
    name='oeislib',
    version='0.0.2',
    description='Simple Python Library for OEIS',
    Long_description=open('README.md').read()+'\n\n'+open('CHANGELOG.txt').read(),
    url='https://gitlab.com/rsholmes/oeislib',
    author='Rich Holmes',
    author_email='rs.holmes@gmail.com',
    License='MIT',
    classifiers=classifiers,
    keywords='oeis',
    packages=find_packages(),
    install_requires=['requests']
)



